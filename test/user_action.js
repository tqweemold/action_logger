'use strict'

const chai = require('chai')
const expect = chai.expect

const chaiHttp = require('chai-http')
chai.use(chaiHttp)

const timeout = 15e3

const app = require('../index')
const request = chai.request(app)

const dummyDoc = {
	user_id: "<uuid>",
	name: "John Due",
	email: "user@example.com",
	user_profile_id: "<uuid>",
	user_profile_role: "user",
	action_name: "update_profile",
	action_description: "update user info",
	action_details: {
		key: "value"
	}
}

describe('habal', () => {
    it('should go smoothly', () => {
        expect(null).to.be.a('null')
    })
})

describe('POST /user_action', () => {
    it('should have status code 406 when missed attributes in the body', function(done) {
        this.timeout(timeout)

        request
            .post('/user_action')
            .send({})
            .end(
                (err, res) => {
                    expect(res).to.have.status(406)

                    done()
                }
            )

    })

    it('should have status code 200 when given right body attributes', function(done) {
        this.timeout(timeout)

        request
            .post('/user_action')
            .send(dummyDoc)
            .end(
                (err, res) => {
                    expect(res).to.have.status(200)

                    done()
                }
            )
    })
})

describe('GET /user_action', (done) => {
    it('should happen without any error', function(done) {
        this.timeout(timeout)

        done()

        request
            .get('/user_action')
            .query()
            .end(
                (err, res) => {
                    expect(err).to.not.exist
                    expect(res).to.have.status(200)
                    expect(res.body.payload).to.be.an('array')

                    done()
                }
            )
    })
})
