const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = class UserAction {
    constructor() {
        const modelSchema = new Schema({
            user_id: {
                type: String,
                require: true,
            },
            name: {
                type: String,
                require: true,
            },
            email: {
                type: String,
                require: true,
            },
            user_profile_id: {
                type: String,
                require: true,
            },
            user_profile_role: {
                type: String,
                require: true,
            },
            action: {
                type: String,
                require: true,
            },
            action_details: {
                type: Object,
                require: true,
                default: null
            },
            created_at: {
                type: Date,
                default: () => Date.now()
            },
        })

        this.Model = mongoose.model('user_action', modelSchema)        
    }


    create(data) {
        return this.Model.create(data)
    }

    read({ criteria = {}, paged = 1, docsPerPage = 20 }) {
        let query = {}

        // date query
        if(criteria.from || criteria.to) {
            query.created_at = {}

            if(criteria.from) {
                query.created_at.$gte = new Date(criteria.from)
            }

            if(criteria.to) {
                query.created_at.$lte = new Date(criteria.to)
            }
        }

        // the rest of the keys
        for(let key of [
            'user_id', 'name', 'email',
            'user_profile_id', 'user_profile_role', 'action'
        ]) {
            if(criteria[key]) {
                query[key] = criteria[key]
            }
        }

        return this.Model.find(query)
            .skip(
                Math.ceil(
                    docsPerPage * (paged - 1)
                )
            )
            .limit(docsPerPage)
    }
}
