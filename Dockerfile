FROM tqweem-node-base

RUN mkdir -p /src
WORKDIR /src
ADD . /src

EXPOSE 3000

CMD ["node", "index.js"]
