const UserAction = require('../models/UserAction')
const action = require('../actions');

module.exports = class LogActionController {
    constructor() {
        this.userActionModel = new UserAction
    }

    render({res, payload, statusCode=200}) {
        res.setHeader('Content-Type', 'application/json')

        return res.status(statusCode).send(
            JSON.stringify(
                { payload }
            )
        )
    }

    create(req, res) {
        req.checkBody({
            user_id: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `user_id can't be empty`
                },
            },
            name: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `name can't be empty`
                },
            },
            email: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `name can't be empty`
                },
                isEmail: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `email format is invalid`
                },
            },
            user_profile_id: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `user profile id can't be empty`
                },
            },
            user_profile_role: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `user profile role can't be empty`
                },
            },
            action: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `action key can't be empty`
                },
            },
            action_details: {
                notEmpty: {
                    options: {
                        checkFalsy: true,
                    },
                    errorMessage: `action details can't be empty`
                },
            },
        })

        const errors = req.validationErrors(true)
        if( !!errors ) {
            return this.render({
                res,
                payload: { errors },
                statusCode: 406,
            })
        }

        let body = {}

        for (let value of [
            'user_id',
            'name',
            'email',
            'user_profile_id',
            'user_profile_role',
            'action',
            'action_details',
        ]) {
            body[value] = req.body[value]
        }

        return this.userActionModel.create( body )
            .then(
                (doc) => {
                    return this.render(
                        {
                            res,
                            payload: doc
                        }
                    )
                },
                (error) => {
                    return this.render(
                        {
                            res,
                            payload: error,
                            statusCode: 500
                        }
                    )
                }
            )
    }

    read(req, res) {
        const { query } =  req
        const paged = query.paged || 1
        const docsPerPage = query.per_page || 20

        let criteria = {}

        // from and to
        for(let key of ['from', 'to']) {
            if(!isNaN(+query[key])) {
                criteria[key] = query[key] * 1000
            } else {
                criteria[key] = query[key]
            }
        }

        // the key
        for(let key of [
            'user_id', 'name', 'email',
            'user_profile_id', 'user_profile_role', 'action'
        ]) {
            if(query[key]) {
                criteria[key] = query[key]
            }
        }

        return this.userActionModel.read({ criteria, paged, docsPerPage }).then(
            (docs) => {
                return this.render(
                    {
                        res,
                        payload: action.transform(req.query.lang, docs)
                    }
                )
            },
            (error) => {
                return this.render(
                    {
                        res,
                        payload: error,
                        statusCode: 500
                    }
                )
            }
        )
    }
}
