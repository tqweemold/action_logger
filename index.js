const app = require('express')();
const validator = require('express-validator')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const swaggerUi = require('swagger-ui-express')
const port = 3000

const {
    NODE_ENV, DB_HOST, DB_NAME,
    DB_USER, DB_PASS, DB_PORT
} = process.env

/*
 * ----------
 * EXPRESS CONFIGURATION
 * ----------
*/
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(validator())

/*
 * ----------
 * SWAGGER
 * ----------
 */
app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(
        require('./swagger.json')
    )
)

/*
 * ----------
 * DATABASE CONNECTION
 * ----------
*/
let dbURI
if (NODE_ENV === 'test') {
    dbURI = `mongodb://localhost/logger`;
} else {
    dbURI = 'mongodb://';

    if (DB_USER && DB_PASS) {
        dbURI = `${dbURI}${DB_USER}:${DB_PASS}@`
    }

    dbURI += DB_HOST;

    if(DB_PORT) {
        dbURI += `:${DB_PORT}`
    }

    dbURI += `/${DB_NAME}`
}

mongoose.Promise = global.Promise
mongoose.connect(dbURI, {
    useMongoClient: true
})

mongoose.connection.on('error',
    console.error.bind(
        console,
        'mongodb connection error:'
    )
)
mongoose.connection.once('open', () => {
    console.log(`mongodb is connected`)
})

/*
 * ----------
 * ROUTES
 * ----------
*/
const LogActionController = require('./controllers/LogActionController')
const logAction = new LogActionController

app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(
        JSON.stringify({
            payload: `the logger server`
        })
    )
})

app.get('/user_action', logAction.read.bind(logAction));
app.post('/user_action', logAction.create.bind(logAction));

if(require.main == module) {
    app.listen(port, () =>{
        console.log(`Logger server is up on port ${port}`);
    })
}

module.exports = app
