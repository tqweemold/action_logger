class Action {
    get(lang, key) {
        let actions;

        try {
            actions = require(`./${lang}.json`);
        }catch(e) {
            throw Error(`Language with ket '${key}' doesn't exists`);
        }

        if(!actions[key]) {
            throw Error(`Key '${key}' doesn't exist`);
        }

        return actions[key];
    }

    transform(lang, docs) {
        let items = JSON.parse(JSON.stringify(docs));

        items.forEach((item) => {
            let action = this.get(lang, 'login');
            item.title = action.title;
            item.description = action.description;
        })
        
        return items;
    }
}

module.exports = new Action;
